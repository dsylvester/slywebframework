/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

app.controller('PasswordRecoveryController', function ($scope) {
    $scope.SecurityQuestions = [];
    $scope.SecurityQuestion = {
        ID: function (id) {
            return angular.isDefined(id) ? (_id = id) : _id;
        },
        Question: function (question) {
            return angular.isDefined(question) ? (_question = question) : _question;
        },
        Response: function (response) {
            return angular.isDefined(response) ? (_response = response) : _response;
        }
    };
    $scope.ShowSecurityQuestions = false;
    
    var _id = 1;
    
    var _SecurityQuestion = function(question, response){
        this.Question = question;
        this.Response = response;
        this.id = _id++;
    }; 
    
    var q1 = new _SecurityQuestion("What street did you grow up on?", "");
    var q2 = new _SecurityQuestion("Who was your first Date?", "");
});

        