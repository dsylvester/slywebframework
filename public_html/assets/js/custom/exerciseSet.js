/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var app = angular.module('WorkoutTrackerExerciseSet', []);

app.controller('ExerciseSetController', function($scope){
   var _id = 1;
   var _weight = '';
   var _reps = '';
   var _difficulty = 1;
   var _setCounter = 1;
   var _exerciseName = '';
   //var exerciseGroup = [];
   
   
   var ES = function(id, wgt, rp, diff){
     this.ID = id;
     this.Weight = wgt;
     this.Reps = rp;
     this.Difficulty = diff;
   };

    $scope.exerciseGroup = [];
    $scope.ExerciseSet = {
        ID: function(id){
      return angular.isDefined(id) ? (_id = id) : _id;
    },      
        Weight: function(weight){
          if ((angular.isDefined(weight))) {
              if (weight < 1 || weight > 2000){
                  _weight = 1;
              }
              else{
                _weight = weight;
            }
            }
            return _weight;
         
    },      
        Reps: function(reps){
          return angular.isDefined(reps) ? (_reps = reps) : _reps;
    },      
        Difficulty: function(difficulty){
          return angular.isDefined(difficulty) ? (_difficulty = difficulty) : _difficulty;
    }
    }; 
    
    $scope.Exercise = {
        Name: function(name){
             if ((angular.isDefined(name))) {
              if (name.length > 0 ){
                  $scope.ExerciseSelected = true;
              }
              _exerciseName = name;
              console.log(_exerciseName);
            }
            return _exerciseName;
        }        
    };
   
    $scope.IncreaseSet = function(){
      _id++;  
    };
   
    $scope.AddExerciseToSet = function($event){
       $event.preventDefault();
       var exerciseSet = new ES(
                $scope.ExerciseSet.ID(),
                $scope.ExerciseSet.Weight(),
                $scope.ExerciseSet.Reps(),                 
                $scope.ExerciseSet.Difficulty());
        $scope.IncreaseSet();       
       
       console.log(exerciseSet);
       $scope.exerciseGroup.push(exerciseSet);
       console.log( JSON.stringify($scope.exerciseGroup, null, 2));
       clearForm();
   };
    $scope.ExerciseSelected = false;
    $scope.ModelHasErrors = function(){
        var repValue = $scope.ExerciseSet.Reps();
        var weightValue = $scope.ExerciseSet.Weight();
        
        if( repValue >= 1 && repValue <= 50  && 
            weightValue >= 1 && weightValue <= 1500
            ){
        return false;
            }
        return true;
    };
    var clearForm = function(){
      _weight = '';
      _reps = '';
      _difficulty = 1;
    };
    
    

});

